import { Component, OnInit, Input } from '@angular/core';
import { TeamMemberService } from '../team-member.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login = '';
  password = '';
  isConnected: boolean;

  constructor(
    private teamMemberService: TeamMemberService, 
    private route: Router
  ) { }

  ngOnInit() {
    console.log('Checking if user is connected from LoginComponent.');
    this.teamMemberService.isUserConnected.subscribe(isConnected => this.isConnected = isConnected);
  }

  authenticate() {
    if (this.login == 'test' && this.password == 'password') {
      console.log('You logged in.');
      this.teamMemberService.changeIsUserConnected(true);
      this.route.navigate(['']);
    } else {
      console.log('Wrong Credentials');
    }
  }

}
