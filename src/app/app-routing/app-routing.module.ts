import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TeamMemberListComponent } from '../team-member-list/team-member-list.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { TeamMemberDetailComponent } from '../team-member-detail/team-member-detail.component';
import { CreateTeamMemberComponent } from '../create-team-member/create-team-member.component';
import { SettingsComponent } from '../settings/settings.component';
import { LoginComponent } from '../login/login.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'team-members',  component: TeamMemberListComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'member/:id', component: TeamMemberDetailComponent },
  { path: 'create-member', component: CreateTeamMemberComponent },
  { path: 'login', component: LoginComponent },
  { path: 'settings', component: SettingsComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
