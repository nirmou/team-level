import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { TEAMMEMBERS } from './mock/team-members-mock';
import { TeamMember } from './model/team-member';

@Injectable()
export class TeamMemberService {
  private isUserConnectedSource = new BehaviorSubject<boolean>(false)
  isUserConnected = this.isUserConnectedSource.asObservable();

  constructor() { }

  changeIsUserConnected(x: boolean){
    console.log('Changing the isUserConnected state');
    this.isUserConnectedSource.next(x);
  }

  createNewTeamMember(member: TeamMember): Observable<TeamMember> {
    // Call a Web service for persisting a new TeamMember
    return of(member);
  }

  getTeamMemberList(): Observable<TeamMember[]> {
    // Call a Web service for getting the list of TeamMember
    return of(TEAMMEMBERS);
  }

  getTeamMember(id: number): Observable<TeamMember> {
    // Call a Web service for getting a specifi TeamMember by its id
    return of(TEAMMEMBERS.find(teamMember => teamMember.id === id));
  }

}
