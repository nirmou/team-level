import { TeamMember } from '../model/team-member';

export const TEAMMEMBERS: TeamMember[] = [
    {
        id: 1,
        imgPath: 'http://via.placeholder.com/150x70',
        firstName: 'Mounir',
        lastName: 'Khanouri',
        skills: [
            {
                name: 'Golang',
                level: 3
            },
            {
                name: 'VueJS 2',
                level: 2
            },
            {
                name: 'PostgreSQL',
                level: 3
            }
        ]
    },
    {
        id: 3,
        imgPath: 'http://via.placeholder.com/150x70',
        firstName: 'Russell',
        lastName: 'Westbrook',
        skills: [
            {
                name: 'C# .NET',
                level: 3
            },
            {
                name: 'Dribbles',
                level: 4
            },
            {
                name: '3 points',
                level: 3
            }
        ]
    },
    {
        id: 4,
        imgPath: 'http://via.placeholder.com/150x70',
        firstName: 'Paul',
        lastName: 'George',
        skills: [
            {
                name: 'Dunk',
                level: 3
            },
            {
                name: 'Rebunds',
                level: 3
            }
        ]
    },
    {
        id: 2,
        imgPath: 'http://via.placeholder.com/150x70',
        firstName: 'Damian',
        lastName: 'Lillard',
        skills: [
            {
                name: 'Step back',
                level: 2
            },
            {
                name: 'Shoots',
                level: 3
            }
        ]
    },
    {
        id: 5,
        imgPath: 'http://via.placeholder.com/150x70',
        firstName: 'Demar',
        lastName: 'Derozan',
        skills: [
            {
                name: 'Javascript',
                level: 4
            },
            {
                name: 'JQuery',
                level: 5
            }
        ]
    },
    {
        id: 6,
        imgPath: 'http://via.placeholder.com/150x70',
        firstName: 'Kyrie',
        lastName: 'Irving',
        skills: [
            {
                name: 'Golang',
                level: 5
            },
            {
                name: 'Web Services',
                level: 4
            },
            {
                name: 'Dribbles',
                level: 5
            }
        ]
    },
    {
        id: 7,
        imgPath: 'http://via.placeholder.com/150x70',
        firstName: 'Lebron',
        lastName: 'James',
        skills: [
            {
                name: 'Java J2EE',
                level: 3
            },
            {
                name: 'Java',
                level: 4
            },
            {
                name: 'Long Range Shots',
                level: 5
            }
        ]
    }
]