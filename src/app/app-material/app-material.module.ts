import { NgModule } from '@angular/core';
import {
  MatToolbarModule,
  MatButtonModule,
  MatGridListModule,
  MatCardModule,
} from '@angular/material';

@NgModule({
  imports: [
    MatToolbarModule,
    MatButtonModule,
    MatGridListModule,
    MatCardModule,
  ],
  exports: [
    MatToolbarModule,
    MatButtonModule,
    MatGridListModule,
    MatCardModule,
  ],
  declarations: []
})
export class AppMaterialModule { }
