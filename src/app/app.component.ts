import { Component, Input } from '@angular/core';
import { TeamMemberService } from './team-member.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isConnected: boolean;

  constructor(
    private teamMemberService: TeamMemberService,
    private route: Router
  ) {}

  ngOnInit() {
    console.log('Checking the user is logged in from AppComponent.')
    this.teamMemberService.isUserConnected.subscribe(isConnected => this.isConnected = isConnected);
  }

  logOut() {
    this.teamMemberService.changeIsUserConnected(false);
    this.route.navigate(['/login']);
  }

}
