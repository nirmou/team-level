export class TeamMember {
    id: number;
    imgPath: string;
    firstName: string;
    lastName: string;
    skills: Skills[];
}

export class Skills  {
    name: string;
    level: number;
}