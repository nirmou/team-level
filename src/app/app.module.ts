import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { TeamMemberListComponent } from './team-member-list/team-member-list.component';
import { TeamMemberDetailComponent } from './team-member-detail/team-member-detail.component';
import { TeamMemberService } from './team-member.service';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppMaterialModule } from './app-material/app-material.module';
import { CreateTeamMemberComponent } from './create-team-member/create-team-member.component';
import { SettingsComponent } from './settings/settings.component';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    TeamMemberListComponent,
    TeamMemberDetailComponent,
    DashboardComponent,
    CreateTeamMemberComponent,
    SettingsComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [
    TeamMemberService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
