import { Component, OnInit } from '@angular/core';
import { TeamMemberService } from '../team-member.service';
import { TeamMember } from '../model/team-member';

@Component({
  selector: 'app-team-member-list',
  templateUrl: './team-member-list.component.html',
  styleUrls: ['./team-member-list.component.css']
})
export class TeamMemberListComponent implements OnInit {
  teamMembers: TeamMember[];

  constructor(private teamMemberService: TeamMemberService) { }
  
  ngOnInit() {
    this.getTeamMemberList();
  }

  getTeamMemberList(): void {
    this.teamMemberService.getTeamMemberList().subscribe(teamMembers => this.teamMembers = teamMembers);
  }

}
