import { Component, OnInit, Input } from '@angular/core';
import { TeamMember } from '../model/team-member';
import { NgForm } from '@angular/forms';
import { TeamMemberService } from '../team-member.service';

@Component({
  selector: 'app-create-team-member',
  templateUrl: './create-team-member.component.html',
  styleUrls: ['./create-team-member.component.css']
})
export class CreateTeamMemberComponent implements OnInit {
  member = new TeamMember();

  constructor(private teamMemberService: TeamMemberService) { }

  ngOnInit() {
  }

  createNewTeamMember(teamMember: TeamMember) {
    this.teamMemberService.createNewTeamMember(teamMember)
      .subscribe(teamMember => this.member = teamMember);
  }

  onSubmit(form: NgForm) {
    console.log(this.member);
    if (form.valid) {
      this.createNewTeamMember(this.member);
    }

    form.reset();
  }

}
