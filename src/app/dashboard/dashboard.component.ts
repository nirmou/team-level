import { Component, OnInit } from '@angular/core';
import { TeamMember } from '../model/team-member';
import { TeamMemberService } from '../team-member.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  teamMembers: TeamMember[] = [];
  isConnected: boolean;

  constructor(private teamMemberService: TeamMemberService) { }

  ngOnInit() {
    this.getTeamMembersList();
    this.teamMemberService.isUserConnected.subscribe(isConnected => this.isConnected = isConnected);
  }

  getTeamMembersList(): void {
    this.teamMemberService.getTeamMemberList().subscribe(teamMembers => this.teamMembers = teamMembers.slice(1,5));
  }

}
